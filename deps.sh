#!/bin/sh


# Announcement
echo "This will install st, slock, and dmenu from my personal builds.
This will require the sudo password to properly install.

Starts in 10 seconds.\n"
sleep 10s

# Getting the git repos
cd ../
[ -d ./st/ ] && echo "Already have st folder." || git clone https://gitlab.com/ahhzee/st.git
[ -d ./slock/ ] && echo "Already have slock folder." || git clone https://gitlab.com/ahhzee/slock.git
[ -d ./dmenu/ ] && echo "Already have dmenu folder." || git clone https://gitlab.com/ahhzee/dmenu.git

# Building st
cd st/
sudo make clean install
rm config.h
# Building slock
cd ../slock/
sudo make clean install
rm config.h
# Building dmenu
cd ../dmenu/
sudo make clean install
rm config.h
# Building dwm
cd ../dwm/
sudo make clean install
rm config.h

echo "Finished building st, slock, dmenu, and dwm from templates."

