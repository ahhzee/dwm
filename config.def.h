/* icons for tags* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 4;        /* border pixel of windows */
static const unsigned int snap      = 16;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = {
	//"Misc Tamsyn:pixelsize=16:antialias=true:autohint=true",
	"UW Ttyp0:pixelsize=16:antialias=true:autohint=true",
	//"xos4 Terminus:pixelsize=16:antialias=true:autohint=true"
	"Noto Color Emoji:pixelsize=16:antialias=true:autohint=true"
};
//static const char dmenufont[]       = "xos4 Terminus:pixelsize=16:antialias=true:autohint=true";
static const char dmenufont[]       = "UW Ttyp0:pixelsize=16:antialias=true:autohint=true";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm]      = { col_gray3, col_gray1,  col_gray2 },
	[SchemeSel]       = { col_gray4, col_cyan,   "#6f6fff" },
	/*
	[SchemeStatus]    = { "#ffffff", col_gray1,  "#000000" }, // Statusbar right {text,background,not used but cannot be empty}
	[SchemeTagsSel]   = { "#ffffff", col_cyan,   "#000000" }, // Tagbar left selected {text,background,not used but cannot be empty}
    [SchemeTagsNorm]  = { col_gray3, col_gray1,  "#000000" }, // Tagbar left unselected {text,background,not used but cannot be empty}
    [SchemeInfoSel]   = { "#ffffff", col_cyan,   "#000000" }, // infobar middle  selected {text,background,not used but cannot be empty}
    [SchemeInfoNorm]  = { col_gray3, col_gray1,  "#000000" }, // infobar middle  unselected {text,background,not used but cannot be empty}
	*/
};

/* tagging */
/* static const char *tags[] = { "   I", "  II", " III", "  IV", "   V", "  VI", " VII", "VIII", "  IX" }; */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     iscentered   isfloating   isterminal noswallow  monitor */
	{ "Steam",    NULL,       "broken",   0,            1,           1,           0,         0,         -1 },
	{ "Steam",    NULL,       "Steam Guard - Computer Authorization Required", 0, 1, 1, 0,   0,         -1 },
	{ "Conky",    NULL,       NULL,       0,            0,           1,           0,         0,         -1 },
	{ "st",       NULL,       NULL,       0,            0,           0,           1,         1,         -1 },
	{ "float",    NULL,       NULL,       0,            0,           1,           0,         1,         -1 },
	{ "cfloat",   NULL,       NULL,       0,            1,           1,           0,         1,         -1 },
	{ "qutebrowser", "qutebrowser", NULL, 0,            0,           0,           0,         1,         -1 },
	{ NULL,       NULL,       "Event Tester", 0,        0,           0,           0,         1,         -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },
	{ "><>",      NULL },
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", "#00ffff", "-sf", "#000000", "-l", "32", "-p", "Run:", "-x", "2170", "-y", "33", "-z", "350", "-i", NULL };
/* Change   "2170"    to 1530 if your moniter is 1080p    *
 * Equasion to figure out is x=RESOLUTION-390             */
static const char *password[] = { "passmenu", NULL };
static const char *quickpad[] = { "st", "-c", "floating", "-t", "quickpad", "-g", "80x70-30-1", "-e", "scratch", NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *xlocker[]  = { "slock", NULL };
static const char *mpdpp[]    = { "mpc", "toggle", NULL };
static const char *mpdsk[]    = { "mpc", "next", NULL };
static const char *mpdup[]    = { "mpc", "volume", "+2", NULL };
static const char *mpddn[]    = { "mpc", "volume", "-2", NULL };
static const char *mpdre[]    = { "mpc", "seek", "0%", NULL };
static const char *volup[]    = { "pactl", "--", "set-sink-volume", "1", "+5%", NULL };
static const char *voldn[]    = { "pactl", "--", "set-sink-volume", "1", "-5%", NULL };
static const char *shtdn[]    = { "quickshut", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_x,      spawn,          {.v = mpdpp} },
	{ MODKEY|ShiftMask,             XK_x,      spawn,          {.v = mpdsk} },
	{ MODKEY|ShiftMask,             XK_c,      spawn,          {.v = mpdre} },
	{ MODKEY|ShiftMask,             XK_v,      spawn,          {.v = mpddn} },
	{ MODKEY,                       XK_v,      spawn,          {.v = mpdup} },
	{ MODKEY|ControlMask,   		XK_v,      spawn,          {.v = volup } },
	{ MODKEY|ControlMask|ShiftMask,	XK_v,      spawn,          {.v = voldn } },
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_grave,  spawn,          {.v = quickpad } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY|ShiftMask,             XK_z,      spawn,          {.v = xlocker } },
	{ MODKEY,                       XK_c,      spawn,          SHCMD("pkill -SIGUSR2 herbe") },
	/*
	{ MODKEY,                       XK_w,      spawn,          {.v = browser } },
	*/
	{ MODKEY,                       XK_w,      spawn,          SHCMD("$BROWSER") },
	{ MODKEY|ShiftMask,             XK_w,      spawn,          {.v = password } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY|ShiftMask,             XK_s,      spawn,          {.v = shtdn } },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ControlMask,           XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} }, //mod+t for tiles
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[13]} },//mod+f for floating
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
	// Floating moveresize.
	{ MODKEY,                       XK_Down,   moveresize,     {.v = "0x 25y 0w 0h" } },
	{ MODKEY,                       XK_Up,     moveresize,     {.v = "0x -25y 0w 0h" } },
	{ MODKEY,                       XK_Right,  moveresize,     {.v = "25x 0y 0w 0h" } },
	{ MODKEY,                       XK_Left,   moveresize,     {.v = "-25x 0y 0w 0h" } },
	{ MODKEY|ShiftMask,             XK_Down,   moveresize,     {.v = "0x 0y 0w 25h" } },
	{ MODKEY|ShiftMask,             XK_Up,     moveresize,     {.v = "0x 0y 0w -25h" } },
	{ MODKEY|ShiftMask,             XK_Right,  moveresize,     {.v = "0x 0y 25w 0h" } },
	{ MODKEY|ShiftMask,             XK_Left,   moveresize,     {.v = "0x 0y -25w 0h" } },
	{ MODKEY|ControlMask,           XK_Up,     moveresizeedge, {.v = "t"} },
	{ MODKEY|ControlMask,           XK_Down,   moveresizeedge, {.v = "b"} },
	{ MODKEY|ControlMask,           XK_Left,   moveresizeedge, {.v = "l"} },
	{ MODKEY|ControlMask,           XK_Right,  moveresizeedge, {.v = "r"} },
	{ MODKEY|ControlMask|ShiftMask, XK_Up,     moveresizeedge, {.v = "T"} },
	{ MODKEY|ControlMask|ShiftMask, XK_Down,   moveresizeedge, {.v = "B"} },
	{ MODKEY|ControlMask|ShiftMask, XK_Left,   moveresizeedge, {.v = "L"} },
	{ MODKEY|ControlMask|ShiftMask, XK_Right,  moveresizeedge, {.v = "R"} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

